FROM docker.io/natsio/nats-box:0.14.3

USER root

ENV S3_ENDPOINT="s3.eu-central-003.backblazeb2.com"
ENV NATS_PORT="4222"
ENV RESTIC_DIR="/etc/restic"
ENV ENABLE_PRUNE=1
ENV KEEP_LAST=100

RUN apk add restic \
    && mkdir $RESTIC_DIR

COPY backup.sh $RESTIC_DIR/

RUN chown -R 1001:1001 $RESTIC_DIR \
    && chmod u+x $RESTIC_DIR/backup.sh

USER 1001

CMD ["/etc/restic/backup.sh"]