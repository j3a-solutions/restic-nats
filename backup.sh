#!/bin/sh

set -euo pipefail

export RESTIC_REPOSITORY=s3:$S3_ENDPOINT/$BUCKET_NAME
export PGPASSWORD=
export SERVER=$NATS_HOST:$NATS_PORT
export BACKUP_DIR=$RESTIC_DIR/nats_dump;
export CACHE_DIR=$RESTIC_DIR/cache

echo "Starting backup with $(restic version)"

echo "Checking snapshots and whether repository exists";
restic snapshots --cache-dir $CACHE_DIR && REPO_EXISTS=$? || REPO_EXISTS=$?

if [ ! $REPO_EXISTS -eq 0 ]
then
  echo "Creating repository in $RESTIC_REPOSITORY"
  restic init;
fi

if [ -z ${NATS_PASSWORD+x} ]; then
  nats -s $SERVER account backup --consumers -f $BACKUP_DIR;
else
  nats -s $SERVER --user=$NATS_USER --password=$NATS_PASSWORD account backup --consumers -f $BACKUP_DIR;
fi

restic backup --cache-dir $CACHE_DIR --verbose $BACKUP_DIR;

echo "NATS account backup added to restic repository";

if [ $ENABLE_PRUNE -eq 1 ]
then
  restic forget --prune --group-by paths --cache-dir $CACHE_DIR --keep-last $KEEP_LAST;
fi