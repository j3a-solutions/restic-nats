#!/bin/sh

set -e

export DOCKER_BUILDKIT=1

DOCKER_REGISTRY=docker.io/j3asolutions/restic-nats
VERSION=v0.2.0
LATEST_TAG=$DOCKER_REGISTRY:latest;
VERSIONED_TAG=$DOCKER_REGISTRY:$VERSION;

docker run --rm --privileged multiarch/qemu-user-static:register --reset
docker buildx create --name nats-restic || true
docker buildx use nats-restic

docker buildx build \
  --file Dockerfile \
  --platform linux/amd64,linux/arm64 \
  --output type=registry \
  --tag "$LATEST_TAG" \
  --tag "$VERSIONED_TAG" \
  --push \
  .

docker buildx rm nats-restic


