# Nats dump backup with restic

This container performs a backup of nats instance by:

- Performing a `nats account backup` of all databases with restic
- Sending the output to remote storage (currently only S3-compatible supported)

## Environment variables

| Variable name         | Description                                                                                                                                      |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| S3_ENDPOINT           | Location of your S3 storage. Defaults to "https://s3.wasabisys.com"                                                                            |
| AWS_ACCESS_KEY_ID     | Key id for s3 storage. Required                                                                                                                  |
| AWS_SECRET_ACCESS_KEY | Key secret for s3 storage. Required                                                                                                              |
| NATS_HOST          | Your NATS host Required                                                                                                                     |
| NATS_PORT          | Defaults to 4222  |
| NATS_USER          | NATS user                                                                                                                    |                  
| NATS_PASSWORD           | Password for NATS account                    |
| BUCKET_NAME           | S3 bucket for your Restic repository. The bucket must exist, but if a Restic repository doesn't, this container will create it for you. Required |
| FILENAME              | Filename/path restic will use for your snapshots. Defaults to 'nats'                                                                          |
| ENABLE_PRUNE          | Whether to forget and prune old snapshots. Value is either 0 or 1. Default is `1`                                                                |
| KEEP_LAST             | How many snapshots to keep. Defaults to 100                                                                                                      |
| RESTIC_PASSWORD       | Restic password env variable. One of RESTIC_PASSWORD or RESTIC_PASSWORD_FILE is required                                                         |
| RESTIC_PASSWORD_FILE  | Restic password file env variable. One of RESTIC_PASSWORD or RESTIC_PASSWORD_FILE is required                                                    |

## Example

1. Run NATS

```bash
docker network create nats
docker run -d --name nats --net nats nats
```

2. Run the NATS dump container

```
docker run --rm \
  -e NATS_HOST=localhost \
  -e BUCKET_NAME=<bucket> \
  -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
  -e RESTIC_PASSWORD=123 \
  --net nats \
  j3asolutions/restic-nats
```

3. Check everything is working fine using restic cli in your local machine


```
RESTIC_REPOSITORY="s3:s3.eu-central-003.backblazeb2.com/<bucket>" \
AWS_ACCESS_KEY_ID=<key> \
AWS_SECRET_ACCESS_KEY=<secret> \
restic -r s3:https://s3.wasabisys.com/<bucket> restore latest --target 
```